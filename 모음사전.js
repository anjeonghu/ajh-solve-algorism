function solution(word) {
  let words = ['A', 'E', 'I', 'O', 'U'];

  let str = '';
  let answer = 0;
  let number = 0;
  const dfs = (str, cnt) => {
    if (cnt === 0) {
      return false;
    }

    let result = words.some((w, idx, origin) => {
      if ((str + w) === word) {
        return true;
      }
      number++;
      return dfs(str + w, cnt - 1);
    });

    return result
  }

  dfs(str, 5);
  return number + 1;
}

console.log(solution('EIO')); // 10이 나와야 한다. I -> 15XX 가 나와야 한다.
