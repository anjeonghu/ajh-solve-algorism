var removeKdigits = function(num, k) {

    if (num.length === k){

        return '0';

    }

    var stack = [];

    var index = 0;

    while (index < num.length) {

        while (k > 0 && stack.length > 0 && Number(stack[stack.length - 1]) >  Number(num.charAt(index))) {
            stack.pop();
            k--;
        }

        stack.push(num.charAt(index));

        index++;

    }

    while (k > 0){
        stack.pop();
        k--
    }
    var result = stack.join('');
    var j = 0;
    while (result.length > 1 && result.charAt(j) === '0') {
        j++;
    }
    result = result.slice(j);
    return result === ''? '0': result;

};


console.log(removeKdigits('1234567890', 9));