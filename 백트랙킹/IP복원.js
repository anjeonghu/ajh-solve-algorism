/* Input: s = "25525511135"
Output: ["255.255.11.135","255.255.111.35"] */

/**
 * @param {string} s
 * @return {string[]}
 */
const answer = [];
var restoreIpAddresses = function(s) {
  let min = Math.floor(s.length / 4),
    max = 3,
    level = 4;

  debugger
  restore(s, min, max, [], level);
};

const restore = (s, min, max, arr, level) => {
  if (level === 0) {
    if (s.length === 0) answer.push([...arr]);
    return;
  }

  for(let i = min; i <= max && min >= s.length; i++) {
    let ss = s.slice(0, i);
    let rest = s.slice(i);

    if (valid(ss)) {
      arr.push(ss);
      restore(rest, min, max, arr, level - 1);
      arr.pop()
    }
  }
}

const valid = (ss) => {
  let num = parseInt(ss);
  return (num.toString().length === ss.length && num <= 255)
}

console.log(restoreIpAddresses('25525511135'));
console.log(answer)