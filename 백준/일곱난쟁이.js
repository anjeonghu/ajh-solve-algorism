/*
왕비를 피해 일곱 난쟁이들과 함께 평화롭게 생활하고 있던 백설공주에게 위기가 찾아왔다. 일과를 마치고 돌아온 난쟁이가 일곱 명이 아닌 아홉 명이었던 것이다.

아홉 명의 난쟁이는 모두 자신이 "백설 공주와 일곱 난쟁이"의 주인공이라고 주장했다. 뛰어난 수학적 직관력을 가지고 있던 백설공주는, 다행스럽게도 일곱 난쟁이의 키의 합이 100이 됨을 기억해 냈다.

아홉 난쟁이의 키가 주어졌을 때, 백설공주를 도와 일곱 난쟁이를 찾는 프로그램을 작성하시오.
*/
// let input = []
// require('readline').createInterface(process.stdin, process.stdout)
//     .on('line', function(line) { input.push(line.trim()) })
//     .on('close', function() {
//         let res = sevenDwarf(input)
//         console.log(res.join('\n'))
//
//     });

const sevenDwarf = (arr) => {
    let dwaf1, dwaf2;
    let sum = arr.reduce((accu, item, index) => {
        return accu + item
    },0)
    sum = sum - 100;
    for(let i = 0; i < arr.length - 1; i++) {
        for(let j = i + 1; j < arr.length; j++) {
            if (sum === (arr[i] + arr[j])) {
                dwaf1 = arr[i];
                dwaf2 = arr[j];
                break
            }
        }
    }
    return arr.filter((item) => {
        return (item !== dwaf1 && item !== dwaf2)
    }).sort((a, b) => a-b);
}
console.log(sevenDwarf([20,
    7,
    23,
    19,
    10,
    15,
    25,
    8,
    13]))
