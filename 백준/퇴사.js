const N = 7;

const t = [3, 5, 1, 1, 2, 4, 2];
const p = [10, 20, 10, 20, 15, 40, 200];
const consult = (t, p) => {
    let maximum = 0;

    for (let i = 0; i < N - 1; i ++) {
        maximum = Math.max(maximum, sum(i, t, p));
    }
    return maximum;
}

const sum = (i, t, p) => {
    if (i + t[i] > N) return 0;
    return p[i] + sum(i + t[i], t, p);
}


console.log(consult(t, p))