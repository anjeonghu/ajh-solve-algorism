/*
* 어떤 양의 정수 X의 각 자리가 등차수열을 이룬다면, 그 수를 한수라고 한다.
* 등차수열은 연속된 두 개의 수의 차이가 일정한 수열을 말한다.
* N이 주어졌을 때, 1보다 크거나 같고, N보다 작거나 같은 한수의 개수를 출력하는 프로그램을 작성하시오.
* */

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}


function main() {
    var N = parseInt(readLine());
    //var b = parseInt(readLine());;

    var res = hansu(N);
    console.log(res);
}



const hansu = (n) => {
    let sum = 0;

    for(let i = 1; i <= n; i++){
        sum += check(i);
    }

    return sum;
}

const check = (num) => {
    let numStr = num.toString();

    if (numStr.length === 1) return 1;

    let diff = Number(numStr[1]) - Number(numStr[0]);
    for(let i = 1; i < numStr.length - 1; i ++) {
        if((Number(numStr[i + 1]) - Number(numStr[i])) !== diff) {
            return 0
        }
    }
    return 1
}
