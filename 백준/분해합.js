/*
어떤 자연수 N이 있을 때, 그 자연수 N의 분해합은 N과 N을 이루는 각 자리수의 합을 의미한다.
어떤 자연수 M의 분해합이 N인 경우, M을 N의 생성자라 한다. 예를 들어, 245의 분해합은 256(=245+2+4+5)이 된다.
따라서 245는 256의 생성자가 된다. 물론, 어떤 자연수의 경우에는 생성자가 없을 수도 있다.
반대로, 생성자가 여러 개인 자연수도 있을 수 있다.

자연수 N이 주어졌을 때, N의 가장 작은 생성자를 구해내는 프로그램을 작성하시오.
* */

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}


function main() {
    var N = parseInt(readLine());
    //var b = parseInt(readLine());;

    var res = bunhaeHap(N);
    console.log(res);
}



const bunhaeHap = (n) => {
    if ((n / 10) <= 0) return n;
    let max = 0;
    for(let i = n; i > 10; i--) {
        let ret = check(n, i);
        if (ret !== 0) {
          max = check(n, i);
        }
    }
    return max;
}

const check = (inputNum, num) => {
    let N = num;
    let sum = 0;
    num = num.toString();
    for(let i = 0; i < num.length; i ++) {
        let n = Number(num.charAt(i));
        sum += n;
    }
    return  ((N + sum) === inputNum) ? N : 0;
}


console.log(bunhaeHap(216))