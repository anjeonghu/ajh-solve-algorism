/* Input: cost = [1,100,1,1,1,100,1,1,100,1] */

/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function(cost) {
  const m_arr = [0, 0];
  const n = cost.length;
  for(let i = 0; i <= n; i++) {
    if(i === 0 || i === 1) {
      m_arr[i] = 0;
      continue;
    }
    m_arr[i] = Math.min(m_arr[i - 1] + cost[i - 1], m_arr[i - 2] + cost[i - 2]);
  }

  return m_arr[m_arr.length - 1];
};