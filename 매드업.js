function solution (queries) {
  let answer = [];

  for (let i = 0; i < queries.length; i++) {
    const game = queries[i];
    player(0, game, answer);
    /*       let arr = [];
          for (let j = 0; j < game.length; j ++) {
            let n = game[j] % 2;
            if (n === 1) {
              arr.push(0);
            } else {
              arr.push(n);
            }

          }
          debugger */
    /*       if(isPelindrome(arr.join(''))) {
           answer.push(1)
          } else {
            answer.push(0)
          } */
    /* console.log(arr) */
  }

  return answer;
}

const player = (p, g, answer) => {
  if(isPelindrome(g.join(''))) {
    let win = p === 0 ? 1 : 0;
    answer.push(p);
    return true;
  }
  for (let i = 0; i< g.length; i++) {
    let gg = [...g];
    if (gg[i]) {
      gg[i]--;
    } else {
      continue;
    }
    if(player(p ^ 1, gg, answer)) return true;
  }
  return false;
}

console.log(solution([[1,4, 3], [1, 2, 2]]))

function isPelindrome(string) {
  let result = true;
  string = string.toUpperCase();

  for (let i = 0; i < Math.floor(string.length / 2); i++) {
    if (string[i] !== string[string.length - i - 1]) {
      return false;
    }
  }

  return result;
}


console.log(isPelindrome('10'))



function solution (info) {
  let answer = [];
  let max = Math.max(...info.flat())
  let arr = new Array(max + 1).fill(0);
  console.log(arr)

  for(let [a1, a2] of info) {
    console.log(a1, a2)
    for (let i = a1; i <= a2; i++) {
      arr[i]++;
    }
  }

  let m = Math.max(...arr);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === m) answer.push(i);
  }
  return answer;
}


console.log(solution([[1, 5], [3, 5], [7, 8]]))