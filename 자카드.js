function solution(str1, str2) {
  var answer = 0;
  const regExp = /[A-Z]{2}/ig;
  //str1 = str1.replace(regExp, '');
  //str2 = str2.replace(regExp, '');

  const ss1 = set(str1.toUpperCase()).filter(s => /[A-Z]{2}/ig.test(s));
  const ss2 = set(str2.toUpperCase()).filter(s => /[A-Z]{2}/ig.test(s));

  console.log(ss1, ss2)

  let intersectionCnt = intersection(ss1, ss2);
  const uu = [...new Set([...ss1, ...ss2])]
  console.log(tempObj(ss1), tempObj(ss2))
  let unionCnt = union(uu, tempObj(ss1), tempObj(ss2));
  /*     const kk = [...new Set(kyo(ss1, ss2))];
      const union = [...new Set([...ss1, ...ss2])]
      answer = Math.floor((kk.length / union.length) * 65536) */
  console.log(intersectionCnt, unionCnt)
  if (unionCnt === 0) return 65536
  answer = Math.floor((intersectionCnt / unionCnt) * 65536)
  return answer;
}
const tempObj = (ss) => {
  const obj = {};
  for (let i = 0; i < ss.length; i++) {
    const s = ss[i];
    if (obj[s]) {
      obj[s]++;
    } else {
      obj[s] = 1;
    }
  }
  return obj;
}

const union = (uu, t1, t2) => {
  let cnt = 0;
  for(let i = 0; i < uu.length; i++) {
    let u = uu[i];
    if(t1[u] && t2[u]) {
      cnt += Math.max(t1[u], t2[u]);
    } else {
      cnt++
    }
  }
  return cnt;
}
const intersection = (ss1, ss2) => {
  let cnt = 0;
  const ss = ss1.length > ss2.length ? [ss2, ss1] : [ss1, ss2];
  const tempObj1 = {};
  for (let i = 0; i < ss[0].length; i++) {
    const s = ss[0][i];
    if (ss[1].indexOf(s) !== -1) {
      cnt++;
    }
  }
  return cnt;
}
const set = (str) => {
  const arr = [];
  for (let i = 1; i < str.length; i++) {
    arr.push(`${str[i - 1]}${str[i]}`)
  }
  return arr;
}

console.log(solution('FRANCE', 'french'));

